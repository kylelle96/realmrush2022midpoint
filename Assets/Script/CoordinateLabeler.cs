using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TMPro;

[ExecuteAlways]
[RequireComponent(typeof(TextMeshPro))]
public class CoordinateLabeler : MonoBehaviour
{
    [SerializeField] Color defaultColor = Color.white;
    [SerializeField] Color blockedColor = Color.gray;
    [SerializeField] Color exploredColor = Color.yellow;
    [SerializeField] Color pathColor = new Color(1f, .5f, 0f); //Orange

    TextMeshPro label;
    GridManager grid;
    Vector2Int coordinate = new Vector2Int();

    private void Awake()
    {
        grid = FindObjectOfType<GridManager>();
        label = GetComponent<TextMeshPro>();
        label.enabled = false;
        DisplayCoordinate();
    }

    private void Update()
    {
        if (!Application.isPlaying)
        {
            DisplayCoordinate();
            UpdateObjectName();
            label.enabled = true;
        }

        SetLabelColor();
        ToggleLabel();
    }

    private void ToggleLabel()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            label.enabled = !label.IsActive();
        }
    }

    private void SetLabelColor()
    {
        if (grid == null) return;

        Node node = grid.GetNode(coordinate);

        if (node == null) return;

        if (!node.isWalkable)
            label.color = blockedColor;
        else if (node.isPath)
            label.color = pathColor;
        else if (node.isExplored)
            label.color = exploredColor;
        else
            label.color = defaultColor;
        
    }


    private void DisplayCoordinate()
    {
        coordinate.x = Mathf.RoundToInt(transform.parent.position.x / grid.UnityGridSize);
        coordinate.y = Mathf.RoundToInt(transform.parent.position.z / grid.UnityGridSize);

        label.text = coordinate.x + ", " + coordinate.y;
    }

    private void UpdateObjectName()
    {
        transform.parent.name = coordinate.ToString();
    }

}
