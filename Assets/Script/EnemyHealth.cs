using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyHealth : MonoBehaviour
{
    [SerializeField] int maxhealth = 5;

    [Tooltip("Add amount to maxHitPoints when enemy dies")]
    [SerializeField] int difficultyRamp = 1;

    private int currentHealth;

    Enemy enemy;

    public int CurrentHealth { get { return currentHealth; } }


    private void OnEnable()
    {
        currentHealth = maxhealth;
    }

    private void Start()
    {
        enemy = GetComponent<Enemy>();
    }

    private void OnParticleCollision(GameObject other)
    {
        ProcessHit();
    }

    private void ProcessHit()
    {
        Damage(1, true);
    }

    public void Damage(int damage, bool isRewarded)
    {
        currentHealth -= damage;
        if(currentHealth <= 0)
        {
            if (isRewarded)
                enemy.RewardGold();
            else
                enemy.StealGold();

            maxhealth += difficultyRamp;
            gameObject.SetActive(false);
        }
    }
}
