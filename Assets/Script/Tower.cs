using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField] int towerCost = 50;
    [SerializeField] [Range(0f, 2f)] float buildTimer;
    public int TowerCost { get { return towerCost; } }

    public static Tower Create(Vector3 position)
    {
        Transform pfTower = Instantiate(GameAssets.Instance.pfTower, position, Quaternion.identity);

        Tower tower = pfTower.GetComponent<Tower>();

        return tower;
    }

    private void Start()
    {
        StartCoroutine(Build());
    }

    private IEnumerator Build()
    {
        foreach(Transform child in transform)
        {
            child.gameObject.SetActive(false);
            foreach(Transform grandChild in child)
            {
                grandChild.gameObject.SetActive(false);
            }
        }


        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
            yield return new WaitForSeconds(buildTimer);
            foreach (Transform grandChild in child)
            {
                grandChild.gameObject.SetActive(true);
            }
        }
       
    }
}
