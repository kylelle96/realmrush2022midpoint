using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField][Range(0.1f, 30)] float spawnTimer = 1f;
    [SerializeField][Range(0, 50)] int poolSize = 5;

    Transform[] poolArray;

    private void Awake()
    {
        PopulatePool();
    }

    private void Start()
    {
        StartCoroutine(SpawnEnemies());
    }

    private void PopulatePool()
    {
        poolArray = new Transform[poolSize];

        for (int i = 0; i < poolArray.Length; i++)
        {
            poolArray[i] = Instantiate(GameAssets.Instance.pfEnemy, transform);
            poolArray[i].gameObject.SetActive(false);
        }
    }

    private void EnableObjectInPool()
    {
        //for (int i = 0; i < poolArray.Length; i++)
        //{
        //    if(!poolArray[i].gameObject.activeInHierarchy)
        //    {
        //        poolArray[i].gameObject.SetActive(true);
        //        return;
        //    }
        //}

        foreach(Transform pool in poolArray)
        {
            if (!pool.gameObject.activeInHierarchy)
            {
                pool.gameObject.SetActive(true);
                return;
            }
        }
    }

    private IEnumerator SpawnEnemies()
    {
        while (true)
        {
            EnableObjectInPool();
            yield return new WaitForSeconds(spawnTimer);

        }
    }

}
