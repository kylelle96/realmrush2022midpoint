using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TargetLocator : MonoBehaviour
{
    [SerializeField] Transform weapon;
    [SerializeField] float targetMaxRadius = 5;
    [SerializeField] float turnSpeed = 2;

    private Transform targetTransform;
    private ParticleSystem towerParticleProjectile;

    private void Awake()
    {
        towerParticleProjectile = transform.Find("BallistaTopMesh").Find("particleProjectile").GetComponent<ParticleSystem>();
        var emissionModule = towerParticleProjectile.emission;
        emissionModule.enabled = false;
    }


    private void Update()
    {
        LookForTarget();
        WeaponLookAtTarget();
        ShootingProjectile();

    }

    private void ShootingProjectile()
    {
        if (targetTransform != null)
        {
            float distance = Vector3.Distance(targetTransform.position, transform.position);
            if (distance < targetMaxRadius)
                Attack(true);
            else
            {
                Attack(false);
                targetTransform = null;
            }
                


        }
    }

    private void Attack(bool hasTarget)
    {
        //towerParticleProjectile.gameObject.SetActive(hasTarget);
        var emissionModule = towerParticleProjectile.emission;
        emissionModule.enabled = hasTarget;
    }

    private void WeaponLookAtTarget()
    {
        if (targetTransform != null)
        {
            Vector3 direction = (targetTransform.position - transform.position).normalized;
            Quaternion rotGoal = Quaternion.LookRotation(direction);
            weapon.rotation = Quaternion.Slerp(weapon.rotation, rotGoal, turnSpeed * Time.deltaTime);
        }
    }


    private void LookForTarget()
    {
        Collider[] colliderArray = Physics.OverlapSphere(transform.position, targetMaxRadius);


        foreach (Collider collider in colliderArray)
        {
            Enemy enemy = collider.GetComponent<Enemy>();

            CheckEnemiesNearby(enemy);

        }
    }

    private void CheckEnemiesNearby(Enemy enemy)
    {
        if(enemy != null)
        {
            if(targetTransform == null)
            {
                targetTransform = enemy.transform;
            }
            else
            {
                //change to the closest target
                if (Vector3.Distance(transform.position, enemy.transform.position) <
                    Vector3.Distance(transform.position, targetTransform.position))
                {
                    targetTransform = enemy.transform;
                }
            }
        }
    }

}
