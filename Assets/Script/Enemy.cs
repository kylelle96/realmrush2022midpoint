using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] int goldReward = 25;
    [SerializeField] int goldPenalty = 25;


    public void RewardGold()
    {
        Bank.Instance.Deposit(goldReward);
    }

    public void StealGold()
    {
        Bank.Instance.Withdrawal(goldPenalty);
        if (Bank.Instance.CurrentBalance <= 0)
        {
            Bank.Instance.ReloadScene();
        }
    }
}
