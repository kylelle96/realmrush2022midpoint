using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyMover : MonoBehaviour
{
    [SerializeField] [Range(0f, 5f)] float speed;

    Enemy enemy;
    EnemyHealth enemyHealth;
    GridManager grid;
    Pathfinder pathfinder;
    List<Node> path = new List<Node>();

    private void Awake()
    {
        enemy = GetComponent<Enemy>();
        enemyHealth = GetComponent<EnemyHealth>();
        grid = FindObjectOfType<GridManager>();
        pathfinder = FindObjectOfType<Pathfinder>();
        
    }

    private void OnEnable()
    {
        ReturnToStart();
        RecalculatePath(true);
       
    }

    private void RecalculatePath(bool resetPath)
    {
        Vector2Int coordinate = new Vector2Int();

        if (resetPath)
        {
            coordinate = pathfinder.StartingCoordinate;
        }
        else
        {
            coordinate = grid.GetCoordinateFromPosition(transform.position);
        }

        StopAllCoroutines();
        path.Clear();
        path = pathfinder.GetNewPath(coordinate);
        if (path.Count > 0)
            StartCoroutine(FollowPath());

    }

    private void ReturnToStart()
    {
        transform.position = grid.GetPositionFromCoordinate(pathfinder.StartingCoordinate);
    }

    private IEnumerator FollowPath()
    {
        for (int i = 1; i < path.Count; i++)
        {
            Vector3 startPosition = transform.position;
            Vector3 endPosition = grid.GetPositionFromCoordinate(path[i].coordinate);
            float travelPercent = 0f;

            transform.LookAt(endPosition);
            while (travelPercent < 1f)
            {
                travelPercent += Time.deltaTime * speed;
                transform.position = Vector3.Lerp(startPosition, endPosition, travelPercent);
                yield return new WaitForEndOfFrame();
            }
        }
        FinishPath();
    }

    private void FinishPath()
    {
        enemyHealth.Damage(9999, false);
    }
}
