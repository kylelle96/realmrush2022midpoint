using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CurrencyUI : MonoBehaviour
{
    private TextMeshProUGUI text;

    private void Awake()
    {
        text = transform.Find("text").GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        Bank.Instance.OnUpdateBalance += Bank_OnUpdateBalance;
        UpdateBalanceAmount();
    }

    private void OnDestroy()
    {
        Bank.Instance.OnUpdateBalance -= Bank_OnUpdateBalance;
    }

    private void Bank_OnUpdateBalance()
    {
        UpdateBalanceAmount();
    }

    private void UpdateBalanceAmount()
    {
        text.SetText(Bank.Instance.CurrentBalance.ToString());
    }
}
