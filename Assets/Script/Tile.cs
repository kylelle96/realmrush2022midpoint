using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [SerializeField] Tower towerPrefab;
    [SerializeField] bool isPlaceable;
    public bool IsPlaceable { get { return isPlaceable; } }

    private GridManager grid;
    private Pathfinder pathfinder;
    private Vector2Int coordinate = new Vector2Int();

    private void Awake()
    {
        grid = FindObjectOfType<GridManager>();
        pathfinder = FindObjectOfType<Pathfinder>();
    }

    private void Start()
    {
        coordinate = grid.GetCoordinateFromPosition(transform.position);

        if (!isPlaceable)
        {
            grid.BlockNode(coordinate);
        }
    }

    private void OnMouseDown()
    {
        if (grid.GetNode(coordinate).isWalkable && towerPrefab.TowerCost <= Bank.Instance.CurrentBalance)
        {
            if (!pathfinder.WillBlockPath(coordinate))
            {
                //GameObject prefab = Instantiate(towerPrefab, transform.position, Quaternion.identity);
                Tower.Create(transform.position);
                Bank.Instance.Withdrawal(towerPrefab.TowerCost);
                isPlaceable = false;
                grid.BlockNode(coordinate);
                pathfinder.NotifyReceiver();
            }
        }
    }
}
