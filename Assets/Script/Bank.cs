using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bank : MonoBehaviour
{
    public static Bank Instance;

    public event Action OnUpdateBalance;

    [SerializeField] int startingBalance = 150;
    int currentBalance;
    public int CurrentBalance { get { return currentBalance; } }

    private void Awake()
    {
        currentBalance = startingBalance;
        Instance = this;
    }

    public void Deposit(int amount)
    {
        currentBalance += Mathf.Abs(amount);
        OnUpdateBalance?.Invoke();
    }

    public void Withdrawal(int amount)
    {
        currentBalance -= Mathf.Abs(amount);
        currentBalance = (int)Mathf.Clamp(currentBalance, 0, Mathf.Infinity);
        OnUpdateBalance?.Invoke();
    }

    public void ReloadScene()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.buildIndex);
    }

}
