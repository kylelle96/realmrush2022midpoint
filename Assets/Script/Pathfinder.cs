using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder : MonoBehaviour
{
    [SerializeField] Vector2Int startingCoordinate;
    public Vector2Int StartingCoordinate { get { return startingCoordinate; } }

    [SerializeField] Vector2Int destinationCoordinate;
    public Vector2Int DestinationCoordinate { get { return destinationCoordinate; } }

    Node startingNode;
    Node destinationNode;
    Node currentSearchNode;

    Queue<Node> frontier = new Queue<Node>();
    Dictionary<Vector2Int, Node> reached = new Dictionary<Vector2Int, Node>();

    GridManager gridManager;
    Dictionary<Vector2Int, Node> grid = new Dictionary<Vector2Int, Node>();
    

    private Vector2Int[] directions = { Vector2Int.right, Vector2Int.left, Vector2Int.up, Vector2Int.down };

    private void Awake()
    {
        gridManager = FindObjectOfType<GridManager>();
        if(gridManager != null)
        {
            grid = gridManager.Grid;
            startingNode = grid[startingCoordinate];
            destinationNode = grid[destinationCoordinate];
        }
        GetNewPath();
    }

    void Start()
    {
       
    }

    public List<Node> GetNewPath()
    {
        return GetNewPath(startingCoordinate);
    }
    public List<Node> GetNewPath(Vector2Int coordinate)
    {
        gridManager.ResetNode();
        BreathFirstSearch(coordinate);
        return BuildPath();
    }

    private void ExploreNeighbors()
    {
        List<Node> neighbors = new List<Node>();

        foreach(Vector2Int direction in directions)
        {
            Vector2Int neighborCoord = currentSearchNode.coordinate + direction;
            if (grid.ContainsKey(neighborCoord))
            {
                neighbors.Add(grid[neighborCoord]);
            }            
        }

        foreach(Node neighbor in neighbors)
        {
            if(!reached.ContainsKey(neighbor.coordinate) && neighbor.isWalkable)
            {
                neighbor.connectedTo = currentSearchNode;
                reached.Add(neighbor.coordinate, neighbor);
                frontier.Enqueue(neighbor);
            }
        }
    }

    private void BreathFirstSearch(Vector2Int cooordinates)
    {
        startingNode.isWalkable = true;
        destinationNode.isWalkable = true;

        frontier.Clear();
        reached.Clear();

        bool isRunning = true;

        frontier.Enqueue(grid[cooordinates]);
        reached.Add(cooordinates, grid[cooordinates]);

        while(frontier.Count > 0 && isRunning)
        {
            currentSearchNode = frontier.Dequeue();
            currentSearchNode.isExplored = true;
            ExploreNeighbors();
            if(currentSearchNode.coordinate == destinationCoordinate)
            {
                isRunning = false;
            }
        }
    }


    private List<Node> BuildPath()
    {
        List<Node> path = new List<Node>();
        Node currentNode = destinationNode;

        path.Add(currentNode);
        currentNode.isPath = true;

        while(currentNode.connectedTo != null)
        {
            currentNode = currentNode.connectedTo;
            path.Add(currentNode);
            currentNode.isPath = true;
        }

        path.Reverse();

        return path;
    }

    public bool WillBlockPath(Vector2Int coordinate)
    {

        if (grid.ContainsKey(coordinate))
        {
            bool previousState = grid[coordinate].isWalkable;
            grid[coordinate].isWalkable = false;
            List<Node> newPath = GetNewPath();
            grid[coordinate].isWalkable = previousState;

            if(newPath.Count <= 1)
            {
                GetNewPath();
                return true;
            }
        }

        return false;
    }

    public void NotifyReceiver()
    {
        BroadcastMessage("RecalculatePath", false, SendMessageOptions.DontRequireReceiver);
    }
}
